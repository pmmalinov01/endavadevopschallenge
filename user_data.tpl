#!/bin/bash -xe

sudo apt-get update -y 

# install docker
sudo curl https://releases.rancher.com/install-docker/17.03.sh | sh
sudo usermod -a -G docker admin

# Place some agent installation here.

wget https://repo.zabbix.com/zabbix/4.0/ubuntu/pool/main/z/zabbix-release/zabbix-release_4.0-3+bionic_all.deb
sudo dpkg -i zabbix-release_4.0-3+bionic_all.deb
sudo apt-get update -y
sudo apt-get install zabbix-agent -y



# Run wordpress container that connects with RDS cluster.
sudo docker run --name test-wordpress --restart=unless-stopped -e WORDPRESS_DB_HOST="${rds_endpoint}" -e WORDPRESS_DB_USER=rdsuser -e WORDPRESS_DB_PASSWORD=testrds -d wordpress


