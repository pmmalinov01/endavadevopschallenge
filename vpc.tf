resource "aws_vpc" "demo" {
  cidr_block           = "10.12.0.0/19"
  enable_dns_hostnames = true

  tags = {
    "Name" = "test-vpc"
  }
}

##  public subnets
resource "aws_subnet" "demo" {
  count = length(var.public_subnets)

  availability_zone = data.aws_availability_zones.all.names[count.index]
  cidr_block        = var.public_subnets[count.index]
  vpc_id            = aws_vpc.demo.id

  tags = {
    "Name" = "public-subnet"
  }
}

## internet gateway
resource "aws_internet_gateway" "gtw" {
  vpc_id = aws_vpc.demo.id

  tags = {
    Name = "terraform-demo"
  }
}

## create routing table
resource "aws_route_table" "rt" {
  vpc_id = aws_vpc.demo.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.gtw.id
  }
}

resource "aws_route_table_association" "demo" {
  count = length(var.public_subnets)

  subnet_id      = aws_subnet.demo[count.index].id
  route_table_id = aws_route_table.rt.id
}

