## Launch config and auto scaling configuration 

resource "aws_launch_configuration" "lc" {
  image_id         = var.ami
  instance_type    = var.instance_type
  key_name         = var.ssh_key_pair
  name_prefix      = "lc"
  security_groups  = [aws_security_group.instance-sg.id]
  user_data_base64 = base64encode(data.template_file.init.rendered)

  associate_public_ip_address = false

  root_block_device {
    delete_on_termination = true
    volume_size           = 10
    volume_type           = "gp2"
  }

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_autoscaling_group" "asg" {
  depends_on           = [aws_rds_cluster.aurora_cluster]
  desired_capacity     = 2
  launch_configuration = aws_launch_configuration.lc.id
  max_size             = 2
  min_size             = 2
  name                 = "asg"
  load_balancers       = [aws_elb.elb.name]
  vpc_zone_identifier  = [aws_subnet.demo[0].id]

  tag {
    key                 = "Name"
    value               = "TestInstance"
    propagate_at_launch = true
  }
}

